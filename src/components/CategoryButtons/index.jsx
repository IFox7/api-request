import React from "react";

import "./categoryButtons.css";

// Елемент кнопки обрання категорії товара. При натисканні на екран виводим обрану категорію
export default function CategoryButtons({ goods, categoryChoose }) {
  const buttonsAray = [];

  goods.map((elem) => {
    return buttonsAray.push(elem.category);
  });
  const buttonsAll = Array.from(new Set(buttonsAray));
 
  return (
    <div className="buttonsWrapper">
      <li onClick={categoryChoose}>All categories</li>
      {buttonsAll.map((elem, index) => {
        return (
          <ul key={index}>
            <li onClick={categoryChoose}>{elem}</li>
          </ul>
        );
      })}
    </div>
  );
}
