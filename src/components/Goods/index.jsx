import React from "react";

import "./goods.css";

// Елемент карточки товара
export default function Goods({ goods }) {
  return (
    <div className="wrapperGoods">
      {goods.map((elem) => {
        return (
          <ul key={elem.id}>
            <li className="itemHeader">{elem.title}</li>
            <ul className="imgDescriptWrapper">
              <img
                className="imgGoods"
                width={100}
                height={100}
                src={elem.image}
                alt={elem.title}
              />
              <li className="descript">{elem.description}</li>
              <li className="count">Quantity to sale:{elem.rating.count}</li>
            </ul>
            <ul className="priceWrapper">
              <li className="rate">Rate:{elem.rating.rate}</li>
              <button>ADD TO CART</button>
              <li className="price">Price:{elem.price}$ </li>
            </ul>
          </ul>
        );
      })}
    </div>
  );
}
