import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App";
// Рендерим основний компонент на сторінці HTML
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
