import React, { Component } from "react";
import Goods from "./components/Goods";
import CategoryButtons from "./components/CategoryButtons";
// Основний елемент
export default class App extends Component {
  // стан основного елемента з урлом,масивами всіх товарів і по категоріях
  state = {
    url: "https://fakestoreapi.com/products",
    goods: [],
    categoryChosen: [],
  };
// Отримуємо дані з ресурса та записуємо їх в стейт
  async componentDidMount() {
    const result = await fetch(this.state.url),
      data = await result.json();

    this.setState((state) => {
      return {
        ...state,
        goods: data,
        categoryChosen: data,
      };
    });
  }

  // Функція фільтрування товарів за категоріями
  categoryChoose = (e) => {
    const category = this.state.goods.filter((elem) => {
      return elem.category === e.target.innerText;
    });

    if (e.target.innerText === "All categories") {
      this.setState((state) => {
        return {
          ...state,
          categoryChosen: this.state.goods,
        };
      });
    } else
      this.setState((state) => {
        return {
          ...state,
          categoryChosen: category,
        };
      });
  };
// Рендер елементів з передаванням їм властивостей через пропси
  render() {
    return (
      <>
        <CategoryButtons
          goods={this.state.goods}
          categoryChoose={this.categoryChoose}
        />
        <Goods goods={this.state.categoryChosen} />
      </>
    );
  }
}
